#!/bin/bash

# Function to check if a process with a given PID is running
is_process_running() {
    local pid=$1
    if ps -p $pid > /dev/null; then
        return 0  # Process is running
    else
        return 1  # Process is not running
    fi
}

# Main loop to check if the process is still running every hour
while true; do
    # Replace X with the PID you want to monitor
    PID_X=1302411

    if is_process_running $PID_X; then
        echo "Process with PID $PID_X is running."
    else
        echo "Process with PID $PID_X is not running. Starting computation..."
        # Replace 'start_computation.sh' with the script you want to run
        ./start_computation.sh
        break  # Exit the loop once the process is not running
    fi

    # Sleep for an hour before checking again
    sleep 3600  # 3600 seconds = 1 hour
done

