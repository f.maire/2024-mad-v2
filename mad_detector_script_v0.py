#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Last modified Sun 24 Sep 2023

write a new todo list
todo:
    Use https://pytorch.org/vision/main/generated/torchvision.ops.nms.html
    to remove duplicate detections in overlapping windows.
    Write a new function process_single_large_image_nms
    (add a parameter iou_thr to this new function)
    First collect the detection bboxes wrt large image
    then perform nms, then save results

Created on Sat Sep  2 22:05:14 2023

@author: frederic,  f.maire@gmail.com

Script to process a folder of large images


"""
import argparse
import time
import itertools

from pathlib import Path
#Path("/my/directory").mkdir(parents=True, exist_ok=True)

import PIL.Image

import torch
import torch.utils.data
# from torch.utils.data.dataset import Dataset

from models import build_model

from torchvision.transforms import v2

from torchvision.transforms.v2 import functional as F


import torchvision.io
# torchvision.io.write_png(input: Tensor, filename: str, compression_level: int = 6)

import os
# import random
# import torchvision
# import copy, json, shutil

import util.misc as utils

# import torchvision.transforms as transforms
import torchvision.transforms as T
from torchvision.utils import draw_bounding_boxes

# from pycocotools.coco import COCO
# from torchvision.datasets.vision import VisionDataset
# import numpy as np
# import skimage.io as io
import matplotlib.pyplot as plt
import pylab
pylab.rcParams['figure.figsize'] = (8.0, 10.0)

from mad_dataset_utils import (
    add_margins,
    # show
    )


# List of the different classes of species
CLASSES = [
    'N/A',
    'turtle',
    'dugong',
    'shark',
    'ray',
    'dolphin',
    'whale',
    'beluga']

MODEL_IIS = 800  # input image side for the model

IMAGE_EXT_LIST = ['jpg','png','gif','bmp','tiff','ppm']
IMAGE_EXT_LIST.extend([ext.upper() for ext in IMAGE_EXT_LIST])

## -------------------- Utils ---------------------------------

# standard PyTorch mean-std input image normalization
# transform = T.Compose([
#     T.Resize(800),
#     T.ToTensor(),
#     T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
# ])

# for output bounding box post-processing
def box_cxcywh_to_xyxy(x):
    x_c, y_c, w, h = x.unbind(1)
    b = [(x_c - 0.5 * w), (y_c - 0.5 * h),
         (x_c + 0.5 * w), (y_c + 0.5 * h)]
    return torch.stack(b, dim=1)

def rescale_bboxes(out_bbox, size):
    img_w, img_h = size
    b = box_cxcywh_to_xyxy(out_bbox)
    b = b * torch.tensor([img_w, img_h, img_w, img_h], dtype=torch.float32)
    return b


# transform to convert a torch image to uint8
transform2Uint8 = v2.ToDtype(torch.uint8)

## -----------------------------------------------------------------------

def make_batch_from_single_image(t_image, # tensor of shape (3,H,W))
                                 side = MODEL_IIS,
                                 overlap = 0.5):
    '''
    Take a large size image, and 

    Parameters
    ----------
    t_image : tensor
        large size image
        
    side : length of the side of the square image
           give as input to the neural network.
           
    overlap : proportion of overlap between the sliding windows
              in one dimension.

    Returns  B, R, C
    -------
    B : a batch of smaller images of shape (3,side, side) that covers 
        the image t_image
    R : list of row coord of top left corner of the sliding windows
    C : list of col coord of top left corner of the sliding windows
    
    That is, B[i,:,:,:] is the ith window and its top left corner has 
      row coord R[i] and col coord C[i]
    '''
    r_max, c_max = t_image.shape[1:]
    # a pixel in the image_np has its coords (r,c) coords satisfying
    #  0 <= r < r_max  and  0 <= c < c_max
    
    r, c = 0, 0 # r row index, c col index  of the top-left corner of the
                # sliding window
    shift = int(side*(1-overlap)) # slide the window by this amount 
    B = [] # batch of windows
    R = []
    C = []
    while r < r_max:
        
        # check whether sliding window is completely in the large image
        if r+side <= r_max and c+side <= c_max :
            # sliding window completely in the large image
            sliding_window = t_image[:,r:r+side,c:c+side]  # .copy() not needed
        else:
            # sliding window overboard!
            sliding_window = torch.zeros((3,side,side), dtype=t_image.dtype)
            n_r = min(r_max - r, side)  # number of pixels we can take in row dim
            n_c = min(c_max - c, side)  # number of pixels we can take in col dim
            sliding_window[:,:n_r, :n_c] =  t_image[:,r:r+n_r,c:c+n_c]
        
        B.append(sliding_window)
        R.append(r)
        C.append(c)
        
        # Slide the window
        # Have we already reached the right side?
        if c+side >= c_max:
            if r_max <= r+side:
                # we are done!
                # we just covered the bottom of the image
                break
            # go to next line
            r += shift
            c = 0            
        else:
            # else slide window to the right
            c += shift
    # return the concatenation of the image windows in B
    return torch.stack(B,axis=0), R, C


## ---------------------------------------------------------------------------

def process_single_large_image(pil_image, #PIL image
                               output_dir,
                               image_path, # the path of pil_image
                               overlap=0.5,
                               min_score_thresh = 0.8):
    '''
    Split the large image into a batch of overlapping windows 
    that can be fed to the detector. Then process the batch

    Update the report text file and save detections images
    
    Parameters
    ----------
    pil_image : PIL image
    output_dir : directory where detection results are saved
    image_path : path of the "large" image being processed
    overlap=0.5 : how much overlap in one direction when the window slides

    Returns
    -------
    None.

    '''
    # Make sure that the image is large enough, pad it if needed.
    
    w, h = pil_image.size 
    bottom_pad =  max(0,MODEL_IIS - h) 
    right_pad =  max(0, MODEL_IIS - w) 
    # need padding?
    if bottom_pad>0 or right_pad>0:
        pil_image = add_margins(pil_image, right_pad, bottom_pad)
    
    t_image = T.ToTensor()(pil_image) # CxHxW format
    
    window_batch, R, C = make_batch_from_single_image(t_image, overlap=overlap)
    # window_batch : a batch of smaller images of shape (3,side, side) that covers 
    #     the image t_image
    # R : list of row coord of top left corner of the sliding windows
    # C : list of col coord of top left corner of the sliding windows
    
    # That is, window_batch[i,:,:,:] is the ith window and its top left corner has 
    #   row coord R[i] and col coord C[i]
    
    
    assert window_batch.shape[1:] == (3, MODEL_IIS, MODEL_IIS)
    
    window_batch = window_batch.to(device)

    outputs = model(window_batch)
    
    result_file = open(Path(output_dir,'detection_results.txt'),'a')
    # don't forget to close me!
    
    result_file.write('\n'+image_path.__str__()+'\n')
  
    im_height, im_width = t_image.shape[1:]

    for i in range(window_batch.shape[0]):
        # if args.verbosity>=3:
        #     print(f'{i}/{window_batch.shape[0]} ',end='\n' if i%21==20 else ' ')
        
        # keep only predictions with min_score_thresh+ confidence
        probas = outputs['pred_logits'].softmax(-1)[i, :, :-1]
        
        # probas: tensor of shape [100,8]
        keep = probas.max(-1).values > min_score_thresh
        # keep is a 1D boolean tensor
        
        # convert boxes from [0; 1] to image scales in xyXY format
        bboxes_scaled = rescale_bboxes(
            outputs['pred_boxes'][i, keep],
            (MODEL_IIS,MODEL_IIS))
        
        # compute the number of detections in the ith window of this
        # large image
        num_detections = keep.count_nonzero()
        
        if num_detections>0:
            # save the detections of this window
            # using bboxes_scaled and probas_keep
            probas_keep = probas[keep]
            t_window_uint8 = transform2Uint8(window_batch[i]*255)
            class_index_list = probas_keep.argmax(dim=-1).tolist()
            labels = [CLASSES[k] for k in class_index_list]
            result_window = draw_bounding_boxes(
                t_window_uint8,
                bboxes_scaled,
                labels=labels,
                colors='red',
                )
            # result_window is a image tensor of dtype uint8

            # if isinstance(image, PIL.Image.Image):
            #     image = F.to_image_tensor(image)
            # image = F.convert_dtype(image, torch.uint8)
            # ax.imshow(annotated_image.permute(1, 2, 0).numpy())
           
            # plt.imshow(result_window.permute(1, 2, 0).numpy())
            
            result_window_path = Path(output_dir, image_path.stem+'_w'+str(i)+'.png')
            torchvision.io.write_png(
                result_window, # tensor
                str(result_window_path), # path as a string
                compression_level = 6)
            
            # Save detections in the text file
            result_file.write('\n'+image_path.stem+'_'+str(i)+'\n') # i : window index
            # bboxes_scaled is in format xyXY, we want yxYX
            bb = bboxes_scaled[:,[1,0,3,2]].tolist()
            # bb list of yxYX boxes
            for j,c in enumerate(class_index_list):
                display_str = f'{CLASSES[c]} : {round(100*probas_keep[j].max().item())}% '
                y,x,Y,X = bb[j]
                display_str += f'{y:.1f},{x:.1f},{Y:.1f},{X:.1f}\n'
                result_file.write(display_str)
                                        
    # close the result file before leaving this function
    result_file.close()
    
        
## ---------------------------------------------------------------------------

def process_main_request():
    '''
    Process the collection passed on the command line.
    
    Preconditions:
        The input specification is stored in the global variable args.input
        The output directory is stored in the global variable args.output

    Returns
    -------
    None.
    '''
    
    # Python notes:
    #     list(p.glob('**/*.py'))
    #     q.is_dir()
    #     q.exists()
    #     with q.open() as f: f.readline()
    #     The “**” pattern means “this directory and all subdirectories, recursively”. In other words, it enables recursive globbing:
    #        Path('.').glob('**/*.py')

    input_path = Path(args.input)
    
    # print(f'{type(input_path)=}') #debug
    #
    if input_path.is_dir():
        # process recursively all folder
        L = [input_path.rglob(''.join(['*.',ext])) for ext in IMAGE_EXT_LIST]        
        for p in itertools.chain(*L):        
            pil_image = PIL.Image.open(p) 
            process_single_large_image(pil_image, 
                                        output_dir= args.output,
                                        image_path = p,
                                        overlap=args.overlap,
                                        min_score_thresh = args.threshold)
    elif input_path.match('*.txt'):
        # we expect a text file containing an image path on each line
        with open(input_path) as f:
            # each line should be a path
            # read the file line by line
            for line in f:
                # skip empty lines and commment lines
                if len(line)==0 or line[0]=='#':
                    continue
                try:
                    p = Path(line)
                    pil_image = PIL.Image.open(p)                   
                except FileNotFoundError as fnf_error:
                    print(fnf_error)
                else:
                    pass
                    process_single_large_image(pil_image, 
                                                output_dir= args.output,
                                                image_path = p,
                                                overlap=args.overlap,
                                                min_score_thresh = args.threshold)
    elif any( input_path.match(''.join(['*.',ext])) for ext in IMAGE_EXT_LIST): 
        # input_path  points to an image
        try:
            pil_image = PIL.Image.open(input_path)                   
        except FileNotFoundError as fnf_error:
            print(fnf_error)
        else:
            process_single_large_image(pil_image, 
                                       output_dir= args.output,
                                       image_path = input_path,
                                       overlap=args.overlap,
                                       min_score_thresh = args.threshold)
                                                    
    
    
## ---------------------------------------------------------------------------


if __name__ == "__main__":
    pass
    # parser.print_help()

    parser = argparse.ArgumentParser()

    description_i ='''
    Input to the program can be an image or a folder or a text file 
    containing a list of folders or image paths.
    '''
    parser.add_argument('-i','--input', help=description_i)
    parser.add_argument('-o','--output',
                        default=os.path.expanduser('~/Detections'),
                        help='Output directory for the results.')
    parser.add_argument('--overlap',
                        default=0.33,
                        type=float,
                        help='Overlap in [0,1) between sliding windows.')
    parser.add_argument('-t','--threshold',
                        default=0.7,
                        type=float,
                        help='Score threshold in [0,1) for filtering the detections.')

    parser.add_argument('-w','--windows', help='if true, save the detection windows')
    



    # Application parameters
    # num_classes is largest class id + 1
    parser.add_argument('--num_classes', default=7,type=int,
                help=' Hard coded number of classes for the neural network ')
    parser.add_argument('--dataset_file', action='store_const', const='')
    
    parser.add_argument('--lr_backbone', default=0, type=float,
                        help='value should be left to 0.0')    

    # * Backbone
    parser.add_argument('--backbone', default='resnet50', type=str,
                        help="Name of the convolutional backbone to use")
    parser.add_argument('--dilation', action='store_true',
                        help="If true, we replace stride with dilation in the last convolutional block (DC5)")
    parser.add_argument('--position_embedding', default='sine', type=str, choices=('sine', 'learned'),
                        help="Type of positional embedding to use on top of the image features")

    # * Transformer
    parser.add_argument('--enc_layers', default=6, type=int,
                        help="Number of encoding layers in the transformer")
    parser.add_argument('--dec_layers', default=6, type=int,
                        help="Number of decoding layers in the transformer")
    parser.add_argument('--dim_feedforward', default=2048, type=int,
                        help="Intermediate size of the feedforward layers in the transformer blocks")
    parser.add_argument('--hidden_dim', default=256, type=int,
                        help="Size of the embeddings (dimension of the transformer)")
    parser.add_argument('--dropout', default=0.1, type=float,
                        help="Dropout applied in the transformer")
    parser.add_argument('--nheads', default=8, type=int,
                        help="Number of attention heads inside the transformer's attentions")
    parser.add_argument('--num_queries', default=100, type=int,
                        help="Number of query slots")
    parser.add_argument('--pre_norm', action='store_true')

    # * Segmentation
    parser.add_argument('--masks', action='store_true',
                        help="Train segmentation head if the flag is provided")

    # Loss
    parser.add_argument('--no_aux_loss', dest='aux_loss', action='store_false',
                        help="Disables auxiliary decoding losses (loss at each layer)")
    # * Matcher
    parser.add_argument('--set_cost_class', default=1, type=float,
                        help="Class coefficient in the matching cost")
    parser.add_argument('--set_cost_bbox', default=5, type=float,
                        help="L1 box coefficient in the matching cost")
    parser.add_argument('--set_cost_giou', default=2, type=float,
                        help="giou box coefficient in the matching cost")
    # * Loss coefficients
    parser.add_argument('--mask_loss_coef', default=1, type=float)
    parser.add_argument('--dice_loss_coef', default=1, type=float)
    parser.add_argument('--bbox_loss_coef', default=5, type=float)
    parser.add_argument('--giou_loss_coef', default=2, type=float)
    parser.add_argument('--eos_coef', default=0.1, type=float,
                        help="Relative classification weight of the no-object class")

    # parser.add_argument('--output_dir', default="output",
    #                     help='path where to save, empty for no saving')
    parser.add_argument('--device', default='cpu',
                        help='device to use for training / testing')
    parser.add_argument('--seed', default=42, type=int)
    # parser.add_argument('--resume', default='', help='resume from checkpoint')

    parser.add_argument('--eval', action='store_const', const=True)
    parser.add_argument('--num_workers', default=2, type=int)


    args = parser.parse_args()
    
    # Zbook
    # args.resume = '/home/frederic/Documents/RESEARCH/2023_MAD_TORCH/detr/output/checkpoint.pth'
    args.resume = '/home/frederic/Documents/RESEARCH/2023_MAD/detr/output/checkpoint_1.pth'
                  # '/home/frederic/Documents/RESEARCH/2023_MAD/detr/output'

    # ................ debugging examples  ................ 
    
    # args.input = '/home/frederic/Documents/shared_data/Dug_Pilbara_Positives_May2018/20180514_c5_fid30_r59/'
    # args.input += 'DJI_0179.JPG'

    # args.input = '/home/frederic/Documents/ready_data/dugong_first250_survey/'
    # args.input += 'i1029AUG12F2F102AFT_DSC9387.jpg'

    # args.input ='/home/frederic/Documents/shared_data/dugong_first250_survey/'
    # args.input += '31AUG12F1F101AFT_DSC3953.ppm'
    
    # .................................................
    
    utils.init_distributed_mode(args)
 
    # needed

    device = torch.device(args.device)

    torch.set_grad_enabled(False);
    
    Path(args.output).mkdir(parents=True, exist_ok=True)    
    
    print("-- Key Args --")
    print(f'{args.input=}')
    print(f'{args.output=}')
    print(f'{args.overlap=}')
    print(f'{args.threshold=}')
    
    print('Restoring neural network model...', end='')

    start_time = time.time()

    # need args.num_classes, args.device
    model, criterion, postprocessors = build_model(args)
        

    # checkpoint = torch.load("detr-r50_no-class-head.pth", map_location='cpu')
    checkpoint = torch.load(args.resume, map_location='cpu')
    model.load_state_dict(checkpoint['model'], strict=False)
    
    # checkpoint = torch.load("detr-r50_no-class-head.pth", map_location='cpu')
    # model.load_state_dict(checkpoint['model'], strict=False)
    model.eval()
    model.to(device)
    
    
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f'\nModel loading took {elapsed_time} seconds\n')
    
    start_time = time.time()
    
    process_main_request()
 
    end_time = time.time()
    elapsed_time = end_time - start_time
    print(f'Processing of request took {elapsed_time} seconds')


