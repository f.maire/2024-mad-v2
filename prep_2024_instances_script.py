'''

Prepare the instances json files by traversing the folder
tree rooted at 
  /media/frederic/DATA/Frederic_MAD/2024_image_datasets

For each folder determine whether there is an object file. 
If there is one, create the corresponding instances json file.

We ensure that each folder has its own json annotation file in 
the format yxYX for the bbox.
The format yxYX means (top, left, bottom, right)

Verify the info by displaying the animals!

The list of species is in the variable CLASSES in mad_dataset_utils.py
 
The "objects.txt" files contains lines of the form
16F2F1_DSC_0119.jpg,dugong,16F2F1_DSC_0119_dugong_3.jpg,{"type":"Polygon","coordinates":[[[2697,1418],[2778,1418],[2778,1592],[2697,1592],[2697,1418]]]}
16F2F1_DSC_0235.jpg,dugong,16F2F1_DSC_0235_dugong_5.jpg,{"type":"Polygon","coordinates":[[[2519,715],[2635,715],[2635,853],[2519,853],[2519,715]]]}

We need to extract 
 - the first field (image file name)
 - the 2nd field (class label)
 - 3rd field can be ignored
 - the 4th field is a sequence of coordinates (x,y) where  
    x represents the horizontal distance and y the vertical distance. 
    
Create a regular expression    
'''



# importing the zipfile module 
from zipfile import ZipFile 
  
from pathlib import Path
# import glob
import json
import sys

import os

import re # regular expression for processing objects.txt files
from PIL import Image

import mad_dataset_utils



def ensure_instances(folder):
    '''
        Check whether the folder contains images.
        If yes, check whether a file "instances.json" is present.
        If no instances.json exists,  check whether there is a txt file 
        with "object" in its name in the folder or its parent. 
        If yes, process this objects.txt to create an instances.json file.
        if no image file is present in the folder 
        return False, otherwise  return True  (folder is an image dataset)
    '''
    folder = Path(folder)
    folder_has_images = mad_dataset_utils.contains_images(folder) 
    if not folder_has_images:
        return False

    print(f"\n\n>>>> Examining image folder -> {folder=}")

    folder_has_tif_images = mad_dataset_utils.contains_images(folder,
                                        image_extensions = ('tif',)) 
    instances_path = folder/'instances.json'
    if instances_path.exists(): # and not folder_has_tif_images :
        # the instances.json file already exists, 
        # nothing to do
        return True
    
    # look for objects text file
    L = list(folder.glob('*object*.txt'))
    
    if len(L)==1:
        # if we reach this point, there is an objects.txt file
        objects_path = L[0]
    else:
        assert len(L)==0 # otherwise multiple objects.txt file!!        
        print(f'No objects.txt in image folder {folder}', file=sys. stderr)  
        print('checking parent folder')
        L = list(folder.parent.glob('*object*.txt'))
        if len(L)!=1:
            print(f'No objects.txt in image folder parent of {folder}', file=sys. stderr)  
            raise Exception(f"objects.txt missing for {folder=}")
        # We found an object file in the parent folder, let's use that one
        objects_path = L[0]
    
    
    instances = {}
    instances['info'] = {"description": "2024 data set, bbox annotations in format yxYX", "version": "1.0"},
    instances['categories'] = mad_dataset_utils.make_categories()
    instances['images'] = []
    instances['annotations'] = []
 
    # the line of the objects.txt file are of the form
    # line_expl = '16F2F1_DSC_0119.jpg,dugong,16F2F1_DSC_0119_dugong_3.jpg,{"type":"Polygon","coordinates":[[[2697,1418],[2778,1418],[2778,1592],[2697,1592],[2697,1418]]]}'

    # regular expression to process a line of an objects.txt file
    # coord_re_s = r"\[\[" + 4 * r"\[(\d+)\.?\d*,(\d+)\.?\d*\]," + r"\[(\d+)\.?\d*,(\d+)\.?\d*\]\]\]\}" 
    
    coord_re = r"\[(\d+)\.?\d*,(\d+)\.?\d*\]"  
 
    # prefix_extractor = re.compile(r"(.+),(\w+),.+:\[\[")
    # changed the re to accept class label like "sea snake"
    # prefix_extractor = re.compile(r"(.+),(.+),.+:\[\[")

    prefix_extractor = re.compile(r"(.+),(.+),.+,\{.+:\[\[")
    
    # map image name to the corresponding dict in instances['images']
    my_image_dict = dict()
    next_image_id = 1
    next_ann_id = 1
    
    with open(objects_path) as obj_file:
        for line in obj_file:
            m = re.match(prefix_extractor,line)   
            try:
                image_name = m.group(1)
            except:
                print(f'faulty line {line}')
                print(f'in folder {folder}')
                raise Exception("Corrupted file according to Fred!!")
            
            # #debug
            # if image_name.endswith('170915-173335-AbovePhotography-0K2A5762.JPG'):
            #                        pass
            
            if image_name.lower().endswith(".tif") or image_name.lower().endswith(".tiff"):
                tif_path = os.path.join(folder, image_name)
                image_name = os.path.splitext(image_name)[0] + ".png" #  new name
                png_path = os.path.join(folder, os.path.splitext(image_name)[0] + ".png")
                
                if not Path(tif_path).exists():
                    print(f'The image {image_name} is missing from {folder}', file=sys. stderr)  
                    continue
                
                try:
                    # Open the TIF image and save it as PNG
                    with Image.open(tif_path) as img:
                        img.save(png_path, format="PNG")
                except:
                    print(f'******  Having issue while processing {tif_path}')
                    print(f'Skipping line {line}')
                    continue
           
            if  not image_name.endswith(('JPG','jpg','JPEG','jpeg','PNG','png')):
                print(f'Ignoring {image_name} because of its non-supported image type', file=sys. stderr)  
                continue
            # image type is ok   

            image_path = folder/image_name
            if not image_path.exists():
                print(f'The image {image_name} is missing from {folder}', file=sys. stderr)  
                continue

            taxon = m.group(2).lower()
            # need to remove spaces
            taxon = taxon.replace(" ","")
            
            if not taxon in mad_dataset_utils.CLASSES[1:]:
                # ignore line because of unknown taxon
                print(f'warning: unknown taxon, skipping {line=}')
                continue
                
            # Create a dict for this image if needed.
            # That is, if it has not already an entry instances['images'])
            if image_name in my_image_dict:
                image_id = my_image_dict[image_name]['id']
            else:
                # new image, create a dictionary
                instances_image_dict = {"file_name":image_name, "id":next_image_id}
                image_id = next_image_id
                next_image_id += 1 # update the index
                # Get the height and with of this large image
                pil_image = Image.open(image_path)
                instances_image_dict['width'] = pil_image.width
                instances_image_dict['height'] = pil_image.height
                # Add instances_image_dict to the list instances['images']
                instances['images'].append(instances_image_dict)
                my_image_dict[image_name] = instances_image_dict
             
            # At this point, instances['images'] and my_image_dict are up-to-date
            # image_id  is valid
            
            # Add the annotation of the line
                                 
            # deal with a variable number of points

            # collect the X (horizontal) and Y (vertical) coords of the vertices
            # X = [ int(m.group(3+2*k)) for k in range(4)]
            # Y = [ int(m.group(4+2*k)) for k in range(4)]
            
            C = re.findall(coord_re, line[m.end():])
            X = [int(p[0]) for p in C]
            Y = [int(p[1]) for p in C]
            ann_dict = dict() # dictionary for the annotation of the current line
            ann_dict['image_id'] = image_id
            ann_dict['bbox'] = [min(Y), 
                                min(X), 
                                min( max(Y), my_image_dict[image_name]['height']-1 ), 
                                min( max(X), my_image_dict[image_name]['width']-1 )]
            ann_dict['category_id'] = mad_dataset_utils.CLASSES.index(taxon)
            ann_dict['id'] = next_ann_id
            next_ann_id += 1
            
            instances['annotations'].append(ann_dict) 
            print(f'new annotation for {image_name=} {taxon=} bbox={ann_dict["bbox"]}')
            
            # do some sanity checks
            if not (
                0 <= ann_dict['bbox'][0] < my_image_dict[image_name]['height']
                    and
                0 <= ann_dict['bbox'][2] < my_image_dict[image_name]['height']
                    and
                0 <= ann_dict['bbox'][1] < my_image_dict[image_name]['width']
                    and
                0 <= ann_dict['bbox'][3] < my_image_dict[image_name]['width']
                ):
                print('@@@@   Crap! @@@@')
                print(f'{folder=}')
                print(f'{image_name=}')
                print(f'{line=}')
                print(f'{X=}, {Y=}')
                print(f'{ann_dict=}')
                print(f"{instances_image_dict['height']=}")
                print(f"{instances_image_dict['width']=}")
                raise Exception('@@@@   Crap in ensure_instances! @@@@')
                      
       
    # save json file
    with open(instances_path, "w") as f:
        json.dump(instances, f)
    
    return True # we just process an image dataset folder

# ----------------------------------------------------------------

def test_walk():
    top = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images'
    
    data_folder_list = []
    for folder, dirs, files in os.walk(top, topdown=True):
        folder = Path(folder)
        instances_path = folder/'instances.json'
        if instances_path.exists():
            # the instances.json file already exists, 
            # nothing to do
            print(f'instances file found in {folder}')
        # look for objects text file
        L = list(folder.glob('*object*.txt'))
        if len(L)==1:
            print(f'found objects  in folder {folder}', file=sys. stderr)  
            data_folder_list.append(folder)
            
    return data_folder_list
 

# ----------------------------------------------------------------

def preparatory_walk(top = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images'):
    '''
     Walk through a tree of folders. Create the list of the
     folders containing image datasets and ensure that each of these folders
     contains and instances.json file (create it from the objects.txt file if
     needed).
     
    '''
        
    # Fred hack 1
    # skip_folders = [Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/Antarctic_minke_whale_2008-2010'),  ]
    # these folders are already done, skip them
    
    data_folder_list = []
    for folder, dirs, files in os.walk(top, topdown=True):
        folder = Path(folder)
        
        # Fred hack 1
        # if folder in skip_folders:
        #     data_folder_list.append(folder)
        #     print(f'Fred Hack 1 : skipping {folder} ')
        #     continue
        
        is_dataset_folder = ensure_instances(folder)
 
        if is_dataset_folder:
            data_folder_list.append(folder)
            
    return data_folder_list
 
# ----------------------------------------------------------------


# ----------------------------------------------------------------

def prepare_train_val_folders(top = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images'):
    '''
        /media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images
        
    '''
    data_folder_list = preparatory_walk(top) # make sure the instances.json files exist

    dst_train = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train'
    dst_val = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/val'
    
    mad_dataset_utils.split_folders(data_folder_list, 
                                    dst_train, 
                                    dst_val)
    
# ----------------------------------------------------------------
def debug_1():
    '''
    aux function, extract some list from a file
    '''
    with open(('/home/frederic/Documents/RESEARCH/MAD_TORCH_23/detr/console_output.txt')) as f:
        for line in f:
            if line.startswith('>>>> Examining image folder -> folder=PosixPath('):
                i = line.find('Path')
                print(line[i:], ',')
                

# ----------------------------------------------------------------


if 0: # Extract all dataset zip files
    # Done on the 22/01/2024
    #
    # Extract all zip files of the form 
    # MAD training images-*.zip
    # located in the folder
    # /home/frederic/Downloads
    # to the location
    # /media/frederic/DATA/Frederic_MAD/2024_image_datasets
    download_path = Path('/home/frederic/Downloads')
    destination_path = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/GBRF dugong surveys')
    # destination_path = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets')
    # zip_list = list(download_path.glob('MAD training images-*.zip'))
    zip_list = list(download_path.glob('images-*.zip'))
    #
    for zip_path in zip_list:
        # loading the temp.zip and creating a zip object 
        with ZipFile(zip_path, 'r') as zObject:           
            # Extracting all the members of the zip  
            # into a specific location. 
            zObject.extractall( path=destination_path ) 
    #        
    # ---------------- zip files have been extracted -------------------------       
            
            
# --------------------------------- main --------------------------------------

if __name__ == '__main__':
    pass

    preparatory_walk("/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/")
    
    
    # prepare_train_val_folders()
    
    
    # code to fix the GBR folder
    # data_folder_list = ["/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/GBRF dugong surveys"]
    # dst_train = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train'
    # dst_val = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/val'    
    # ensure_instances(data_folder_list[0])
    # mad_dataset_utils.split_folders(data_folder_list, 
    #                                 dst_train, 
    #                                 dst_val)

    
    
    # debug_1()
    # D = test_walk()
    # # test ensure_instances function
    
    # test_folder = "/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/NESP 2023 - reviewed up to Jan 2024/230608_AM_16_Port_KR_export/trainingsdata-2023_12_10-16_11/images"
    # # ensure_instances(test_folder)
    # sfid = mad_dataset_utils.SingleFolderImageDataset(test_folder)
    # for k in range(len(sfid)):
    #     ii, tt = sfid[k]
    #     mad_dataset_utils.show((ii,tt))
    



# +++++++++++++++++++++++  CODE CEMETARY +++++++++++++++++++++++
            
# line_expl = '16F2F1_DSC_0119.jpg,dugong,16F2F1_DSC_0119_dugong_3.jpg,{"type":"Polygon","coordinates":[[[2697,1418],[2778,1418],[2778,1592],[2697,1592],[2697,1418]]]}\n'


# # extractor = re.compile(r"(.+),(\w+),.+,\{(.+)\}")
# coord_re_s = r"\[\[" + 4 * r"\[(\d+),(\d+)\]," + r"\[(\d+),(\d+)\]\]\]\}" 
# extractor = re.compile(r"(.+),(\w+),.+"+coord_re_s)

# m = re.match(extractor,line_expl)

# image_name = m.group(1)
# taxon = m.group(2).lower()
# X = [ int(m.group(3+2*k)) for k in range(4)]
# Y = [ int(m.group(4+2*k)) for k in range(4)]


    # m.group(1)
    # Out[17]: '16F2F1_DSC_0119.jpg'
    
    # m.group(2)
    # Out[18]: 'dugong'
    
    # m.group(3)
    # Out[19]: '"type":"Polygon","coordinates":[[[2697,1418],[2778,1418],[2778,1592],[2697,1592],[2697,1418]]]'

