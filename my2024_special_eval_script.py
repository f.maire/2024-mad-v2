#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 15:36:20 2024

@author: frederic

Ad hoc test of the trained NN on Val folder

todo:
    run mad detector script and verify the results in 
    the text file
    
/home/frederic/Documents/Atelier
    
"""
import PIL.Image
import itertools
from pathlib import Path

from collections import defaultdict

import mad_dataset_utils
from mad_detector_script import IMAGE_EXT_LIST

src_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Val/')
output_dir = Path('/home/frederic/Detections/')

try:
    src_sfdid = mad_dataset_utils.SingleFolderDynamicImageDataset(src_folder)
except:
    print(f'issues with {src_folder=}')
    raise Exception('Error while creating src_sfdid')

# gg = [src_folder.glob(''.join(['*.',ext])) for ext in IMAGE_EXT_LIST]

# L = list(itertools.chain(*gg))

present_image_ids = list(src_sfdid.present_image_ids)

confusion = defaultdict(int)
# confusion[(c1,c2)] is a counter of how many c1 objects
# were classified as c2   
# species are defined in mad_dataset_utils.CLASSES


with open(Path(output_dir,'detection_results.txt')) as f:
           dr_lines = f.readlines()

# create mapping image_name -> list of detections
map_detect = defaultdict(list)

ann_first_line_read = False
for line in dr_lines:
    if line.startswith('/'):
        # new image
        image_stem = Path(line).stem
        ann_first_line_read = False
    elif line.startswith(image_stem):
        # annotation first line
        ann_first_line_read = True
    elif len(line)>1:
        ann_first_line_read = False
        # annotation 2nd line
        line_chunks = line.split()
        pred_species = line_chunks[0]
        yxYX = line_chunks[3].split(',')
        y,x,Y,X = (int(v) for v in yxYX)
        map_detect[image_stem].append({'bbox':[y,x,Y,X],"category_name":pred_species})

    

for pii in present_image_ids:
    ann_ids = src_sfdid.coco.getAnnIds(pii)
    anns = src_sfdid.coco.loadAnns(ann_ids)
    im_d = src_sfdid.coco.loadImgs(pii) # image dict
  
    
  
    
    # open 
# for p in L:        
#     pil_image = PIL.Image.open(p) 
#     process_single_large_image(pil_image, 
#                                 output_dir= args.output,
#                                 image_path = p,
#                                 overlap=args.overlap,
#                                 min_score_thresh = args.threshold)

