#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 30 17:23:34 2023

Last modified on Mon 31 July 2023
- fix bugs

@author: frederic, f.maire@gmail.com

Script demonstrating the module mad_dataset_utils


The module mad_dataset_utils contains classes and functions 
to prepare a PyTorch dataset from a collection of folders containing 
jpg or png images and each folder with its own json annotation file 
in the format xyXY for the bbox.

The format xyXY means (top, left, bottom, right)
However, some authors use x for the left-right axis and y 
for the top-bottom axis. Be carefull!!


The sequence of steps to process each folder is a follows
- Step 1 : Ensure each animal is of diameter smaller than the input of 
  the neural network with the function shrink_dataset_images_if_needed
- Step 2 : Ensure each image is at least ready_image_side x ready_image_side 
  where ready_image_side is the size of the input of the neural network.
  Typically, ready_image_side = 800.
  This step is done by padding on the right and the bottom of the 
  images if needed using the function pad_dataset_images_if_needed.

After the first two steps, we have a collection of folders with 
appropriate annotated images. 

A training set and a validation set can be created
by calling ... 


"""

print("*************** OBSOLETE CODE *************** ")
import torch.utils.data

import pathlib

import torchvision.transforms as transforms

from mad_dataset_utils import (
    shrink_dataset_images_if_needed,
    pad_dataset_images_if_needed,
    SingleFolderImageDataset,
    show,
    split_folders
    )
             

# size of the input image of the neural detector
ready_image_side = 800

# proportion of examples used for validation
val_ratio = 0.15

raw_dataset_folder_list = [                    
        '/home/ahmed/MAD/Mad_dataset/original_folders/DBCA_WA_humpback_whales_aerial',
        '/home/ahmed/MAD/Mad_dataset/original_folders/Dug_Positives_Pilbara_dec20',
        '/home/ahmed/MAD/Mad_dataset/original_folders/humpback_relabel_hodgson',
        '/home/ahmed/MAD/Mad_dataset/original_folders/Hyperoodon_planifrons',
        '/home/ahmed/MAD/Mad_dataset/original_folders/Tassie_Minke_whales',
        '/home/ahmed/MAD/Mad_dataset/original_folders/whales_collection_1',
        '/home/ahmed/MAD/Mad_dataset/original_folders/whales_collection_2',
        '/home/ahmed/MAD/Mad_dataset/original_folders/whales_collection_3',
        '/home/ahmed/MAD/Mad_dataset/original_folders/Whales_from_v1',
        '/home/ahmed/MAD/Mad_dataset/original_folders/Whales_from_v2',
        ]

shrink_dataset_folder_list = [
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/DBCA_WA_humpback_whales_aerial',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/Dug_Positives_Pilbara_dec20',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/humpback_relabel_hodgson',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/Hyperoodon_planifrons',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/Tassie_Minke_whales',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/whales_collection_1',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/whales_collection_2',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/whales_collection_3',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/Whales_from_v1',
        '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/Whales_from_v2',
        ]

pad_dataset_folder_list = [
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/DBCA_WA_humpback_whales_aerial',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/Dug_Positives_Pilbara_dec20',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/humpback_relabel_hodgson',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/Hyperoodon_planifrons',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/Tassie_Minke_whales',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/whales_collection_1',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/whales_collection_2',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/whales_collection_3',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/Whales_from_v1',
        '/home/ahmed/MAD/Mad_dataset/padded_dataset/Whales_from_v2',
        ]


# if 1:
#     for raw_dataset_folder, shrink_dataset_folder in zip(
#             raw_dataset_folder_list,
#             shrink_dataset_folder_list
#             ):
#         shrink_dataset_images_if_needed(raw_dataset_folder, 
#                                         shrink_dataset_folder,
#                                         ready_image_side = ready_image_side)
        
# if 1:
#     for shrink_dataset_folder, pad_dataset_folder in zip(
#             shrink_dataset_folder_list,
#             pad_dataset_folder_list            
#             ):
#         pad_dataset_images_if_needed(shrink_dataset_folder, 
#                                         pad_dataset_folder,
#                                         ready_image_side = ready_image_side)

# # split into training and validation collections    
# if 1:    
#     split_folders( 
#             pad_dataset_folder_list, # list of the dataset folders
#             dst_train = '/home/ahmed/MAD/Mad_dataset/mad_dataset_ready/train', # root folder where the training folders will be created
#             dst_val = '/home/ahmed/MAD/Mad_dataset/mad_dataset_ready/val', # root folder where the validation folders will be created
#             val_ratio = val_ratio)
    

# Combine the training datasets
if 1:    
    train_root_path = pathlib.Path(
        '/home/ahmed/MAD/Mad_dataset/mad_dataset_ready/train')
    cid = torch.utils.data.dataset.ConcatDataset(
                    [
                        SingleFolderImageDataset(dataDir) 
                        for dataDir in train_root_path.iterdir() 
                        if dataDir.is_dir()
                    ])
    
    
    print(f"Number of examples in combined dataset {len(cid)}")

# if 1:
#     # Extract the 9th training example of the combined dataset
#     image, target = cid[738] 
#     show((image,target))

