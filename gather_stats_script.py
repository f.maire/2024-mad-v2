#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 15:39:18 2024

@author: frederic
"""
from pathlib import Path
#Path("/my/directory").mkdir(parents=True, exist_ok=True)

# import PIL.Image
import matplotlib.pyplot as plt

import numpy as np
from math import sqrt
# import torch
# import torch.utils.data
# from torch.utils.data.dataset import Dataset

# import os
# import random
# import torchvision

# import copy, json, shutil

import mad_dataset_utils

# ep = Path('/home/frederic/Documents/RESEARCH/MAD_TORCH_23/detr/output/eval/')

# dd = torch.load(ep/'650.pth')



def gather_statistics(sfid):
    
    # Get categories and their IDs
    # coco_dataset = sfid.coco.dataset

    # categories = coco_dataset.loadCats(coco_dataset.getCatIds())
    category_names = [mad_dataset_utils.CLASSES[k] for k in range(1,len(mad_dataset_utils.CLASSES))]
    
    # category_ids = [category['id'] for category in categories]

    # Initialize dictionary to store statistics for each category
    category_statistics = {category_name: {'count': 0, 'diameters': []} for category_name in category_names}

    # Get annotations
    # annotations = coco_dataset.loadAnns(coco_dataset.getAnnIds())
    
    annotations = [ann for ann in sfid.coco.dataset['annotations'] if ann['id'] in sfid.present_ann_ids]

    # Gather statistics for each annotation
    for annotation in annotations:
        category_id = annotation['category_id']
        # category_name = next(category['name'] for category in categories if category['id'] == category_id)
        category_name = mad_dataset_utils.CLASSES[category_id]

        # Update count for category
        category_statistics[category_name]['count'] += 1

        # Calculate bounding box size (area)
        bbox = annotation['bbox']
        dy, dx = abs(bbox[2]-bbox[0]), abs(bbox[3]-bbox[1])
        diam = sqrt(dy*dy + dx*dx)

        # Add size to the list of sizes for the category
        category_statistics[category_name]['diameters'].append(diam)

    # Calculate size statistics for each category
    for category_name in category_statistics.keys():
        sizes = category_statistics[category_name]['diameters']
        if sizes:
            category_statistics[category_name]['mean_size'] = np.mean(sizes)
            category_statistics[category_name]['median_size'] = np.median(sizes)
            category_statistics[category_name]['min_size'] = np.min(sizes)
            category_statistics[category_name]['max_size'] = np.max(sizes)
        else:
            category_statistics[category_name]['mean_size'] = None
            category_statistics[category_name]['median_size'] = None
            category_statistics[category_name]['min_size'] = None
            category_statistics[category_name]['max_size'] = None

    return category_statistics

# Example usage

dataDir = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Train')

sfid = mad_dataset_utils.SingleFolderImageDataset(dataDir)

        # # Compute the list of annotations ids of present images
        # self.present_ann_ids = [
        #     ann['id'] for ann in self.coco.dataset['annotations']
        #             if ann["image_id"] in self.present_image_ids
        #     ]


category_statistics = gather_statistics(sfid)

for category_name, stats in category_statistics.items():
    print(f"Category: {category_name}")
    print(f"Number of annotations: {stats['count']}")
    print(f"Mean size: {stats['mean_size']}")
    print(f"Median size: {stats['median_size']}")
    print(f"Min size: {stats['min_size']}")
    print(f"Max size: {stats['max_size']}")
    print()


# Calculate size statistics for each category
for category_name in category_statistics.keys():
    sizes = category_statistics[category_name]['diameters']
    if len(sizes)==0:
        print(f'Warning: no data for class {category_name}')
        continue # ski
    category_statistics[category_name]['mean_size'] = np.mean(sizes)
    category_statistics[category_name]['median_size'] = np.median(sizes)
    category_statistics[category_name]['min_size'] = np.min(sizes)
    category_statistics[category_name]['max_size'] = np.max(sizes)
    
    # Compute histogram of area distribution
    sizes = category_statistics[category_name]['diameters']
    plt.figure(figsize=(8, 6))
    plt.hist(sizes, bins=30, color='skyblue', edgecolor='black')
    plt.title(f'Diameter Distribution for {category_name}')
    plt.xlabel('Diameter')
    plt.ylabel('Frequency')
    plt.grid(True)
    plt.savefig("./output/hist_"+category_name+'.jpg')
    plt.show()


