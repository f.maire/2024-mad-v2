#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 15:36:20 2024

@author: frederic

Ad hoc test of the trained NN on Val folder

todo:
    run mad detector script and verify the results in 
    the text file
    
/home/frederic/Documents/Atelier
    
"""
import PIL.Image
import itertools
from pathlib import Path
import copy, json, shutil

from collections import defaultdict

import mad_dataset_utils
from mad_detector_script import IMAGE_EXT_LIST

from mad_dataset_utils import CLASSES # the species

atelier = '/home/frederic/Documents/Atelier/'

# src_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Val/')
# output_dir = Path('/home/frederic/Detections/')

# try:
#     src_sfdid = mad_dataset_utils.SingleFolderDynamicImageDataset(src_folder)
# except:
#     print(f'issues with {src_folder=}')
#     raise Exception('Error while creating src_sfdid')

# gg = [src_folder.glob(''.join(['*.',ext])) for ext in IMAGE_EXT_LIST]

# L = list(itertools.chain(*gg))

# present_image_ids = list(src_sfdid.present_image_ids)

confusion = defaultdict(int)
# confusion[(c1,c2)] is a counter of how many c1 objects
# were classified as c2   
# species are defined in mad_dataset_utils.CLASSES


with open(Path(atelier+'instances.json')) as f:
    instances = json.load(f)



with open(Path(atelier+'detection_results.txt')) as f:
           dr_lines = f.readlines()

# create mapping image_name -> list of detections
map_detect = defaultdict(list)


def compute_intersection(rect1, rect2):
    """
    Compute the intersection area of two rectangles.
    """
    y1, x1, Y1, X1 = rect1
    y2, x2, Y2, X2 = rect2
    
    intersection_y1 = max(y1, y2)
    intersection_x1 = max(x1, x2)
    intersection_Y2 = min(Y1, Y2)
    intersection_X2 = min(X1, X2)
    
    if intersection_Y2 <= intersection_y1 or intersection_X2 <= intersection_x1:
        return 0
    
    return (intersection_Y2 - intersection_y1) * (intersection_X2 - intersection_x1)

def compute_union(rect1, rect2, intersection):
    """
    Compute the union area of two rectangles.
    """
    area_rect1 = (rect1[2] - rect1[0]) * (rect1[3] - rect1[1])
    area_rect2 = (rect2[2] - rect2[0]) * (rect2[3] - rect2[1])
    
    return area_rect1 + area_rect2 - intersection

def compute_iou(rect1, rect2):
    """
    Compute the Intersection over Union (IoU) of two rectangles.
    """
    intersection_area = compute_intersection(rect1, rect2)
    union_area = compute_union(rect1, rect2, intersection_area)
    
    if union_area == 0:
        return 0
    
    return intersection_area / union_area



def compare_anns(gt, pred):
    '''
    Upate the confusion dictionary
    
    gt : ground truth annotations for an image (from instances.json)
    pred : detections by the NN for the same image 

    '''
    for a in gt:
        detected = False
        for p in pred:
            if compute_iou(a['bbox'],p['bbox'])>= 0.5:
                confusion[(CLASSES[a['category_id']], p['category_name'])] += 1
                detected = True
        if not detected:
            confusion[(CLASSES[a['category_id']], 'N/A')] += 1
            
    # false positive accounting
    for p in pred:
        if all(compute_iou(a['bbox'],p['bbox']) < 0.5 for a in gt):
            # false detection
            confusion[( 'N/A', p['category_name'])] += 1
    

image_stem = 'sentinel value'
for line in dr_lines:
    if line.startswith('/'):
        # new image
        image_stem = Path(line).stem
    elif line.startswith(image_stem):
        # annotation first line
        pass # notthing to do
    elif len(line)>1:
        # annotation 2nd line
        line_chunks = line.split()
        pred_species = line_chunks[0]
        yxYX = line_chunks[3].split(',')
        y,x,Y,X = (int(v) for v in yxYX)
        map_detect[image_stem].append({'bbox':[y,x,Y,X],"category_name":pred_species})

    
for im_dict in instances["images"]:
    im_stem = Path(im_dict['file_name']).stem
    im_id = im_dict['id']
    # get all ground truth (GT) annotations of this image
    gt_anns = [ann for ann in instances['annotations'] if ann['image_id']==im_id]
    # compare gt_anns and map_detect['im_stem'] 
    compare_anns(gt_anns, map_detect[im_stem])

    
print(confusion)


# for pii in present_image_ids:
#     ann_ids = src_sfdid.coco.getAnnIds(pii)
#     anns = src_sfdid.coco.loadAnns(ann_ids)
#     im_d = src_sfdid.coco.loadImgs(pii) # image dict
  
    
  
    
    # open 
# for p in L:        
#     pil_image = PIL.Image.open(p) 
#     process_single_large_image(pil_image, 
#                                 output_dir= args.output,
#                                 image_path = p,
#                                 overlap=args.overlap,
#                                 min_score_thresh = args.threshold)

