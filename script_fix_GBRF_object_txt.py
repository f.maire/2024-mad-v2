#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 29 15:00:13 2024

@author: frederic
"""

from pathlib import Path
# import re # regular expression for processing objects.txt files
# from PIL import Image


# obj_path = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/GBRF dugong surveys/images/objects.txt')

def process_text_file(input_file, output_file):
    try:
        with open(input_file, 'r') as infile:
            with open(output_file, 'w') as outfile:
                for line in infile:                    
                    modified_line = line.replace('\n','')[1:-1].replace('""', '"')
                    outfile.write(modified_line+"\n")

        print(f"Processing complete. Results saved to {output_file}")

    except FileNotFoundError:
        print(f"Error: File '{input_file}' not found.")

if __name__ == "__main__":
    input_file_path = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/GBRF dugong surveys/images/objects.txt')
    output_file_path = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/GBRF dugong surveys/images/new_objects.txt')
    process_text_file(input_file_path, output_file_path)

