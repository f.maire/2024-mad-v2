#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 20 14:41:03 2023

@author: ahmed
"""
import json
import random
import time
from pathlib import Path

import numpy as np
import torch
from torch.utils.data import DataLoader, DistributedSampler

import datasets
import util.misc as utils
from datasets import build_dataset, get_coco_api_from_dataset

import mad_dataset_utils

dataset_train = torch.utils.data.dataset.ConcatDataset(
                [
                    mad_dataset_utils.SingleFolderImageDataset(dataDir) 
                    for dataDir in 
                    [
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_DBCA_WA_humpback_whales_aerial',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_Dug_Positives_Pilbara_dec20',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_humpback_relabel_hodgson',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_Hyperoodon_planifrons',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_Tassie_Minke_whales',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_whales_collection_1',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_whales_collection_2',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_whales_collection_3',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_Whales_from_v1',
                    '/home/ahmed/MAD/Mad_dataset/shrinked_dataset/train/shrink_Whales_from_v2',
                    ]
 
                ])

X =dataset_train[62]