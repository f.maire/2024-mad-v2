#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 14:52:46 2024

Script to check that the instances.json files are ok

@author: frederic
"""

import argparse
import datetime
import json
import random
import time
from pathlib import Path

import numpy as np
import torch
from torch.utils.data import DataLoader, DistributedSampler

# import datasets
# import util.misc as utils
# from datasets import build_dataset, get_coco_api_from_dataset
# from engine import evaluate, train_one_epoch
# from models import build_model
import mad_dataset_utils
import pathlib

try_path_1 = pathlib.Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train')
# try_path_1 = pathlib.Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/val')


if try_path_1.exists():
    train_root_path = try_path_1
    

# dataset_train = torch.utils.data.dataset.ConcatDataset(
#                 [
#                     mad_dataset_utils.SingleFolderImageDataset(dataDir) 
#                     for dataDir in train_root_path.iterdir() 
#                     if dataDir.is_dir()
#                 ])


def verify(sfid):
    
    for index in range(len(sfid)):
        # pa : present annotation referred by index
        pa = sfid.coco.loadAnns(sfid.present_ann_ids[index])[0]        
        
        # index is wrt to self.present_ann_ids
        id = pa["image_id"] # id of the large image 
        # print('image id is',pa)
        image_l = sfid._load_image(id) # the large image
                                    # the type of image_l is class 'PIL.Image.Image'
        target_l = sfid._load_target(id) # the list of anns of this large image
                           # it calls self.coco.loadAnns(self.coco.getAnnIds(...))
                           # target_l is a list of annotations
                           
        y_i, x_i, Y_i, X_i = pa['bbox'] # format y,x,Y,X
        try:
            # assert 0<= y_i < Y_i < image_l.height
            assert Y_i - y_i < 800
            # assert 0<= x_i < X_i < image_l.width
            assert X_i - x_i < 800
        except:
            # print("------ BAD EXAMPLE ----")

            print("------ Animal TOO LARGE ----")
            print(f"{pa=}")
            print(f"{image_l.width=}, {image_l.height=}" )
            print(f"{target_l}")
            # raise Exception("bad example!!")


# L = [
#      Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train/Dug_Positives_Pilbara_dec20'),
#      Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train/images_39'),
#      Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train/humpback_relabel_hodgson'),
# Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/train/DBCA_WA_humpback_whales_aerial'),

# ]

for dataDir in train_root_path.iterdir():
    
# L = [Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/hidden place for naughty folders/train-images_39'),]
 
# for dataDir in L:
    if dataDir.is_dir() :
        print(f"\n ....... Considering {dataDir=}")
        sfid = mad_dataset_utils.SingleFolderImageDataset(dataDir) 
        print(f"{len(sfid)=}")
        
        verify(sfid)
        # break   



