#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 14:13:25 2024

Last updated Thu Mar 7
 
@author: frederic
"""

from pathlib import Path
import copy, json, shutil
import random

from PIL import Image, ImageDraw




import mad_dataset_utils

def black_out_rectangle(image, x, X, y, Y):
    '''
    Draw a black rectangle defined by  x,X,y,Y on the PIL image    
    
    Parameters
    ----------
    image : PIL image

    x: min horizontal coord (left)
    X: max horizontal coord (right)
    y: min vertical coord (top)
    Y: max vertical coord (bottom)
    Returns
    -------
     new image of type PIL 

    '''
    # Convert image to RGBA mode to support transparency
    image = image.convert("RGBA")

    # Create a drawing object
    draw = ImageDraw.Draw(image)

    # Draw a black rectangle
    draw.rectangle([x, y, X, Y], fill=(0, 0, 0, 255))

    # Convert image back to original mode (e.g., "RGB")
    image = image.convert("RGB")

    return image


def blackout_rectangles(image, R, L):
    """
    Black out all rectangles in list L and preserve the region under rectangle R in the image.
    
    Args:
    image (PIL.Image): The input image.
    R (tuple): Rectangle coordinates in the form of (x1, y1, x2, y2).
    L (list): List of rectangles where each rectangle is represented as (x1, y1, x2, y2).
    
    Returns:
    PIL.Image: The modified image with blacked-out rectangles.
    """
    # Create a mask to preserve the region under rectangle R
    # PIL.Image.new(mode, size, color=0)
    # mode 'L' (8-bit pixels, grayscale)
    mask = Image.new('L', image.size, 255)
    draw = ImageDraw.Draw(mask)
    
    # Black out all rectangles in list L
    for rect in L:
        draw.rectangle(rect, fill=0)

    draw.rectangle(R, fill=255)
    
    # Apply the mask to the original image
    result = Image.new('RGB', image.size) # contruct a black image
    result.paste(image, mask=mask)
    
    return result

def test_bo():
    im = Image.open("/home/frederic/Pictures/this_guy.jpg")
    R = (500,500,1000,800)
    L = [(200,200,600,600),(900,400,1200,800)]
    im_r = blackout_rectangles(im, R, L)
    im_r.show()

def make_static_dataset(src_folder, dst_folder):
    '''
 
    Create a "static" dataset of images 800x800 from the 
    images located in src_folder. The new dataset images will be
    located in dst_folder as well as the new instances.json file.
    
    src_folder : str or Path
    dst_folder : str or Path

    Returns
    -------
    None.

    '''
    print(f'>>> Preparing static dataset for {src_folder=}')
    
    try:
        src_sfdid = mad_dataset_utils.SingleFolderDynamicImageDataset(src_folder)
    except:
        print(f'issues with {src_folder=}')
        raise Exception('Error while creating src_sfdid')
        
    # Make sure that the folder for the static dataset exist
    dst_folder = Path(dst_folder)
    dst_folder.mkdir(parents=True, exist_ok=True)
    
    # create a new instances.json file
    
    with open(Path(src_folder,"instances.json")) as f:
        in_instances = json.load(f)
        
    out_instances = {}
    out_instances['info'] = copy.deepcopy(in_instances['info'])
    out_instances['categories'] = copy.deepcopy(in_instances['categories'])
    out_instances['images'] = []
    out_instances['annotations'] = []
    
    ready_image_side = mad_dataset_utils.READY_IMAGE_SIDE

    # consider all annotations in images present in src_folder
    for k, ia in enumerate(src_sfdid.present_ann_ids):
        # ia: index of a annotation in an image present in the src_folder
        # pa : present annotation with id = ia
        # k : is an enumeration counter
        pa = src_sfdid.coco.loadAnns(ia)[0]  # the annotation corresponding with id=ia  
        
        id_0 = pa["image_id"] # id of the large image 
        # print('image id is',pa)
        image_0_dict = src_sfdid.coco.imgs[id_0]
        image_l = src_sfdid._load_image(id_0) # the large image
                                    # the type of image_l is class 'PIL.Image.Image'
        target_l = src_sfdid._load_target(id_0) # the list of anns of this large image
                           # it calls self.coco.loadAnns(self.coco.getAnnIds(...))
                           # target_l is a list of annotations
        
        assert len(target_l)>0
        
        # show((image_l,target_l))   # debug
        
        # Create a random rectange rr that contains the 
        # bounding box of the annotation pa
        #  random.randint(a, b) returns a random integer N 
        #       such that a <= N <= b.
        # The topleft corner of the rr window is (y_w, x_w)
        large_image_width, large_image_height = image_l.size
        assert ready_image_side <= large_image_width
        assert ready_image_side <= large_image_height
        
        y_i, x_i, Y_i, X_i = pa['bbox'] # format y,x,Y,X
        
        # Fred hack, to address problem of some mislabels
        X_i, Y_i =  min(large_image_width-1, X_i), min(large_image_height-1,Y_i)
        y_i, Y_i = min(y_i, Y_i,), max(y_i, Y_i)
        x_i, X_i = min(x_i, X_i), max(x_i, X_i)
        
        if not 0 <= x_i < X_i < large_image_width:
            print(f'{target_l=} {x_i=} {X_i=} {large_image_width=}')
            print(f'{image_0_dict=}')
            raise Exception("shit happens!")

        if not 0 <= y_i < Y_i < large_image_height:
            print(f'{target_l=} {y_i=} {Y_i=} {large_image_height=}')
            print(f'{image_0_dict=}')
            raise Exception("shit happens!")
            
        try:
            ax, bx = max(0, X_i - ready_image_side) , min(x_i, large_image_width - ready_image_side)
            x_w = random.randint(min(ax,bx) ,max(ax,bx))                       
            # x_w = random.randint(
            #         max(0, X_i - ready_image_side) ,
            #         min(x_i, large_image_width-1 - ready_image_side)
            #         )            
            ay, by = max(0, Y_i - ready_image_side) , min(y_i, large_image_height - ready_image_side)
            y_w = random.randint(min(ay,by) , max(ay,by))    
                    
            # y_w = random.randint(
            #         max(0, Y_i - ready_image_side) ,
            #         min(y_i, large_image_height-1 - ready_image_side)
            #         )
            # # rr : the random rectangular window containing the pa bbox
            X_w = min(x_w+ready_image_side, large_image_width-1)
            Y_w = min(y_w+ready_image_side, large_image_height-1)            
            #    rr = np.array([x_w, y_w, X_w, Y_w],dtype=np.int32)
        except:
            print('---------------')
            print(f'{target_l=}  {x_i=} {X_i=}  {large_image_width=}')
            print(f'{target_l=} {y_i=} {Y_i=} {large_image_height=}')            
            print(f'{image_0_dict=}')
            print('---------------')
            raise Exception("shit happens!")

        # debug assert statements
        if not (0<=x_w) and (x_w<=x_i) and (X_i<=X_w) and (X_w<large_image_width):
            print(f'{target_l=} {x_w=} {x_i=} {X_i=} {X_w=} {large_image_width=}')
            print(f'{image_0_dict=}')
            raise Exception("shit happens!")

            
        if not (0<=y_w) and (y_w<=y_i) and (Y_i<=Y_w) and (Y_w<large_image_height):
            print(f'{target_l=} {y_w=} {y_i=} {Y_i=} {Y_w=}  {large_image_height=}')
            print(f'{image_0_dict=}')
            raise Exception("shit happens!")
            

        image_w = image_l.crop([x_w,y_w,X_w,Y_w])

        # ki will be the image id of image_w in  out_instances['images']
        ki = len(out_instances['images'])
        image_w_width, image_w_height = image_w.size

        image_w_dict = dict(file_name=str(ki)+image_0_dict["file_name"] ,
                            height = image_w_height,
                            width = image_w_width,
                            id = ki
                            )
        out_instances['images'].append(image_w_dict) # using ki for image_w

        # Examing how the bounding boxes intersect rr
        L = [] # list of (x1, y1, x2, y2) rectangle to be blacked out
        R = (x_i-x_w, X_i-x_w, y_i-y_w, Y_i-y_w)
        for ann_j in target_l:
            #    The intersection of rr with box j is y_q,x_q, Y_q, X_q 
            #    The coords x_q,y_q, X_q, Y_q are wrt the large image
            y_j,x_j, Y_j, X_j  = ann_j['bbox']                 
            # if this bbox is not completely contained in rr, black it out
            
            # y_q,x_q, Y_q,X_q  are coords wrt large image
            y_q, Y_q = max(y_w,y_j), min(Y_w,Y_j) 
            x_q, X_q = max(x_w,x_j), min(X_w,X_j) 

            if not ( (x_w <= x_j <= X_j <= X_w) and (y_w <= y_j <= Y_j <= Y_w) ):
                # if not fully contained, simply black out ann_j
                # todo: if bbox j overlap bbox i, preserve bbox i 
                # old code: image_w = black_out_rectangle(image_w, x_q-x_w, X_q-x_w, y_j-y_w, Y_j-y_w)
                # don't include this bbox in training image
                L.append((x_j-x_w,y_j-y_w,X_j-x_w,Y_j-y_w))
                continue
            
            #  If we reach this point  ann_j is contained in rr
            # WARNING coords in bbox B are relative to image_w 
            B = [y_q-y_w,x_q-x_w,Y_q-y_w,X_q-x_w]
            ann_j_clone = copy.deepcopy(ann_j)
            ann_j_clone['bbox'] = B
            # print('original ann is',ann_j_clone)
            ka = len(out_instances['annotations'])
            ann_j_clone['image_id'] = ki
            ann_j_clone['id'] = ka
            out_instances['annotations'].append(ann_j_clone)
            if not all(0<=v<=ready_image_side for v in B):
                print(f'{target_l=} {ann_j=} {B=} ')
                print(f'{image_0_dict=}')
                raise Exception("shit happens!")            
        # end loop on ann_j
        
        if L: # test whether len(L)>0?
            # black out the partial rectangles
            image_w = blackout_rectangles(image_w, R, L)

        image_w.save(dst_folder/image_w_dict['file_name'])
        
        print(f'Done {k} out of {len(src_sfdid.present_ann_ids)}', end='\r')
        # image_l.crop(box) Returns a rectangular region from this image.
        # aka the "ready image"
        # The box is a 4-tuple defining the left, upper, right, and lower pixel 
        
    # end of loop on present_ann_ids
    
    # save json file
    with open(dst_folder/"instances.json", "w") as f:
        json.dump(out_instances, f)
    

# --------------------------------- main --------------------------------------

if __name__ == '__main__':
    pass

    
    # test_bo()
    
    random.seed(42)
     
    if 0:
        print('>> Starting the preparatory walk!\n')
        src_data = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images')
        # tree of folders some containing dataset images
        folder_list = mad_dataset_utils.preparatory_walk(src_data)
        print('<< Preparatory walk done!\n')
        
    
        print('>> Creation of raw single dataset folder!\n')
        raw_single_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Raw_single_dataset')    
        mad_dataset_utils.merge_dataset_folders(folder_list, raw_single_folder)
        print(f'<< {raw_single_folder=}  created!\n')
        
        print('>> Creation static dataset!\n')
        raw_single_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Raw_single_dataset')    
        static_single_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Static_single_dataset')    
        make_static_dataset(raw_single_folder, static_single_folder)
        print(f'<< {static_single_folder=}  created!\n')
    

    static_single_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Static_single_dataset')    
    print('>> Create training and validation datasets!\n')
    sfid = mad_dataset_utils.SingleFolderImageDataset(static_single_folder)
    train_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Train')    
    val_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Val')    
    sfid.split(train_folder, val_folder, val_ratio=0.05)
    print(f'<< {train_folder=}  and {train_folder=} created!\n')
    
    print('>> Clean instances.json training!\n')
    sfid = mad_dataset_utils.SingleFolderImageDataset(train_folder)
    # sfid.clean_instances()
    sfid.clean_instances(force_no_crowd=True)
    print(f'<< instances.json in {train_folder=}  cleaned!\n')
    
    # print('>> Clean instances.json validation!\n')
    # sfid = mad_dataset_utils.SingleFolderImageDataset(val_folder)
    # sfid.clean_instances()
    # print(f'<< instances.json in {val_folder=}  cleaned!\n')

    print('>> Force no_crowd to 1 instances.json validation!\n')
    val_folder = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/Val') 
    sfid = mad_dataset_utils.SingleFolderImageDataset(val_folder)
    sfid.clean_instances(force_no_crowd=True)
    print(f'<< instances.json in {val_folder=}  cleaned!\n')
    exit()


# ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
    # static_train = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/static_train')
    
    # src_val = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/val')
    # static_val = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/static_val')

    # for src, dst in [(src_train,static_train) , (src_val,static_val )]:
    #     for d in src.iterdir():
    #         if d.is_dir():
    #             if Path(dst/d.name).exists():
    #                 # already processed, skip!
    #                 continue
    #             make_static_dataset(d, dst/d.name)
    
#     static_train = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/static_train')
#     merged_train = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/merged_train')

#     static_val = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/static_val')
#     merged_val = Path('/media/frederic/DATA/Frederic_MAD/2024_image_datasets/merged_val')

#     folder_list = list(static_train.iterdir())
#     dst_folder = merged_train
#     mad_dataset_utils.merge_dataset_folders(folder_list, dst_folder)

#     folder_list = list(static_val.iterdir())
#     dst_folder = merged_val
#     mad_dataset_utils.merge_dataset_folders(folder_list, dst_folder)

# # test code
# if 0: 
#     src_folder = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images/dugong_sea_survey_first250_ready_V'
#     dst_folder = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/static/try_1'
#     make_static_dataset(src_folder, dst_folder)


# # test code for black_out_rectangle()
# if 0:
#     # Example usage:
#     # Replace "path/to/your/image.jpg" with the path to your image file
#     image_path = "/home/frederic/Dropbox/funny pics/BFC/Polish-Silver-Laced-Frizzle-tim-flach-1.jpg"
#     image_w = Image.open(image_path)
    
#     # Specify the rectangle coordinates [x_j, X_j, y_j, Y_j]
#     x_j, X_j, y_j, Y_j = 100, 300, 50, 150
    
#     # Black out the specified rectangle
#     result_image = black_out_rectangle(image_w, x_j, X_j, y_j, Y_j)
    
#     # Display or save the result_image as needed
#     result_image.show()
#     # To save the result_image, uncomment the following line
#     # result_image.save("path/to/save/result_image.jpg")    
    
