#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

Script to create a small dataset that can be used
as a validation dataset

todo: torch.save the subsetdataset
check image
use it in main!

Created on Sat Aug 26 20:43:57 2023

@author: frederic


"""
import torch

import pathlib

import matplotlib.pyplot as plt
# import matplotlib.patches as patches
from torch.utils.data import DataLoader

from util import box_ops

from torchvision. utils import draw_bounding_boxes
from torchvision.transforms.transforms import ConvertImageDtype

import mad_dataset_utils

# from torchvision.datasets import CocoDetection
# from torch.utils.data import ConcatDataset, Subset

import random

# -----------------    user defined variables (edit values)  -----------

val_root_path = pathlib.Path('/home/frederic/Documents/RESEARCH/2023_MAD/mad_dataset_ready/val')
num_samples_to_extract = 100

small_val_dataset_path = pathlib.Path('/home/frederic/Documents/RESEARCH/2023_MAD/mad_dataset_ready/small_val_dataset.pt')
# --------------------------------------------------------------

def show_image_with_boxes(image, target):
    '''
    image: a batch of a single image as a tensor 
           (b,c,h,w)   with b=1 and c=3 when called
    target a dict of tensors including
          target['boxes'] of size (b,n,4) where n is the 
          number of bboxes and b=1      
    '''
    # img = image.squeeze(0).permute(1, 2, 0).numpy()
    # plt.imshow(img)
    
    img = ConvertImageDtype(torch.uint8)(image.squeeze(0))
    # img tensor (3,h,w) of dtype uint8
    
    # convert bboxes to [x0, y0, x1, y1] format 
    out_bbox = target['boxes'][0]
    # target['boxes'][0]  is a n by 4 tensor cx, cy, w, h
    
    boxes = box_ops.box_cxcywh_to_xyxy(out_bbox) 

    # and from relative [0, 1] to absolute [0, height] coordinates 

    img_h, img_w = img.shape[1:] 
    scale_fct = torch.tensor([[img_w, img_h, img_w, img_h]])

    # scale_fct = torch.stack([torch.tensor([img_w]),
    #                          torch.tensor([img_h]),
    #                          torch.tensor([img_w]),
    #                          torch.tensor([img_h])], dim=1)
    #                          # img_w, img_h, img_w, img_h], dim=1) 

    boxes = boxes * scale_fct
    boxes = boxes.to(torch.int)

    annotated_image = draw_bounding_boxes(img,boxes, width=3, colors=(255,255,0))

    plt.imshow(annotated_image.permute(1, 2, 0).numpy())
    plt.show()    
 
    

    
# --------------------------------------------------------------


dataset_val = torch.utils.data.dataset.ConcatDataset(
                [
                    mad_dataset_utils.SingleFolderImageDataset(dataDir) 
                    for dataDir in val_root_path.iterdir() 
                    if dataDir.is_dir()
                ])

# Select a fixed number of training examples randomly
num_total_samples = len(dataset_val)
selected_indices = random.sample(range(num_total_samples), num_samples_to_extract)


# Create a subset dataset using the selected indices
subset_dataset = torch.utils.data.dataset.Subset(dataset_val, selected_indices)

if 1:
    # Save subset_dataset to a pt file  
    small_val_dataset_path.parent.mkdir(parents=True, exist_ok=True)
    example_list = [subset_dataset[k] for k in range(len(subset_dataset))]
    torch.save(example_list, small_val_dataset_path)  

    loaded_dataset = torch.load(small_val_dataset_path)

if 0:
    # Visualize subset_dataset     
    # Create a DataLoader to iterate through the dataset
    data_loader = DataLoader(subset_dataset, batch_size=1, shuffle=False)
    
    # it = iter(data_loader)
    # images, targets = next(it)
    # show_image_with_boxes(images, targets)
    
    # Loop through the dataset and display images with bounding boxes
    for batch in data_loader:
        images, targets = batch
        show_image_with_boxes(images, targets)
        
        # Add a mechanism to close the plot after each image is displayed
        input("Press Enter to continue...")
    
# ii, tt = subset_dataset[3]

# img = ii.permute(1,2,0).numpy()
# plt.imshow(img)
# plt.show()



# # Define a function to save the dataset to a JSON file
# def save_dataset_to_file(val_root_path, filename):
#     data = []
#     for idx in range(len(dataset)):
#         img_info, target = dataset[idx]
#         data.append({"image_id": img_info["id"], "image": img_info["file_name"], "annotations": target})
    
#     with open(filename, 'w') as f:
#         json.dump(data, f)

    


# # Set the paths to your COCO dataset annotations and images
# ann_file = 'path_to_annotations_file.json'
# img_root = 'path_to_images_folder/'

# # Load the COCO dataset
# coco_dataset = CocoDetection(root=img_root, annFile=ann_file)

# # Save the subset dataset to a JSON file
# new_dataset_filename = 'new_dataset.json'
# save_dataset_to_file(subset_dataset, new_dataset_filename)


# print(f"New dataset with {num_samples_to_extract} samples saved to {new_dataset_filename}")
