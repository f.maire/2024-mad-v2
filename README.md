**MAD v2**: an application of detr
========


This project is an application of [detr](https://github.com/facebookresearch/detr) to marine animal detection.

Please check the original **detr** website for technical information on the structure of the deep neural network used in the detection pipeline.


For details see [End-to-End Object Detection with Transformers](https://ai.facebook.com/research/publications/end-to-end-object-detection-with-transformers) by Nicolas Carion, Francisco Massa, Gabriel Synnaeve, Nicolas Usunier, Alexander Kirillov, and Sergey Zagoruyko.



# Usage - Object detection
There are no extra compiled components in DETR and package dependencies are minimal,
so the code is very simple to use. We provide instructions how to install dependencies via conda.
First, clone the repository locally:
```
git clone https://github.com/facebookresearch/detr.git
```
Then, install PyTorch 1.5+ and torchvision 0.6+:
```
conda install -c pytorch pytorch torchvision
```
Install pycocotools (for evaluation on COCO) and scipy (for training):
```
conda install cython scipy
pip install -U 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'
```
That's it, should be good to train and evaluate detection models.


If you are a conda user, my conda environment for this project has been uploaded as "mad2_env.yml". 
To duplicate this environment, type on your console
```
conda env create -f mad2_env.yml
```


## Detection


Download the checkpoint.pth file and move it in the folder called "output". This folder is in the same directory as the script below.
https://drive.google.com/file/d/1YNcJif3jgqDNz5B9vNfM1_aSEpDPZf45/view?usp=sharing (last uploaded on the 2024/April****/06)


Run the script named 
```
python mad_detector_script_py -i ImageCollection -o ResultFolder
```
where 
- ImageCollection can be an image or a folder or a text file  containing a list of folders or image paths
- ResultFolder is the folder where results will be saved

There are other arguments (overlap, threshold and windows) that you can change in the main section of the script. You should consider experimenting with the detection threshold value ('-t' argument) to change the tradeoff between recall and precision. 

# License
DETR is released under the Apache 2.0 license. Please see the [LICENSE](LICENSE) file for more information.

# Contributing
We actively welcome your pull requests! Please see [CONTRIBUTING.md](.github/CONTRIBUTING.md) and [CODE_OF_CONDUCT.md](.github/CODE_OF_CONDUCT.md) for more info.
