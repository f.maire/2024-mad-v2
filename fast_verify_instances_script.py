#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 14:29:51 2024

Faster algorithm to verify the instances.json file
@author: frederic
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  9 14:52:46 2024

Script to check that the instances.json files are ok

@author: frederic
"""

import json
from pathlib import Path

import os

# import numpy as np

# import datasets
# import util.misc as utils
# from datasets import build_dataset, get_coco_api_from_dataset
# from engine import evaluate, train_one_epoch
# from models import build_model

import mad_dataset_utils


def verify_json(folder):
    '''
    folder : Path object
    return True if fine, False otherwise
    '''
    instances_path = folder/'instances.json'
    if not instances_path.exists():
        print(f'MISSING instances.json in {folder}')
        return True
    # load json
    instances_path = folder/'instances.json'
    with open(instances_path) as jf:
        instances = json.load(jf)

    annotations = instances.get("annotations", [])
    images = instances.get("images", [])
    
    if len(annotations)==0:
        print(f'According to instances.json, no annotations in {folder}')
    if len(images)==0:
        print(f'According to instances.json, no images in {folder}')

    shit_happened = False
    for image_info in images:
        image_id = image_info["id"]
        file_name = image_info["file_name"]
        image_path = folder/file_name
        if image_path.exists():        
            for annotation in annotations:
                if annotation["image_id"] == image_id:                    
                    y_i, x_i, Y_i, X_i = annotation['bbox'] # format y,x,Y,X
                    try:
                        assert 0<= y_i < Y_i <= image_info["height"]
                        assert Y_i - y_i < 600
                        assert 0<= x_i < X_i <= image_info["width"]
                        assert X_i - x_i < 600
                    except:
                        # print("------ BAD EXAMPLE ----")            
                        print("------ Animal TOO LARGE ----")
                        print(f"{folder=}")
                        print(f"{file_name=}")
                        cat = mad_dataset_utils.CLASSES[annotation['category_id']]
                        print(f"{annotation['bbox']=} {cat=}" )
                        shit_happened = True
    return not shit_happened

    
# 
# 
# --------------------------------- main --------------------------------------

if __name__ == '__main__':
    pass

    top = '/media/frederic/DATA/Frederic_MAD/2024_image_datasets/MAD training images'
    train_root_path = Path(top)
    
    
    data_folder_list = []
    for folder, dirs, files in os.walk(top, topdown=True):
        folder = Path(folder)
        folder_has_images = mad_dataset_utils.contains_images(folder) 
        if not folder_has_images:
            continue
        ok = verify_json(folder)
        if not ok:
            data_folder_list.append(folder)

    # print list of problematic folders
     
    print(f"\n\n {data_folder_list=}")


# dataset_train = torch.utils.data.dataset.ConcatDataset(
#                 [
#                     mad_dataset_utils.SingleFolderImageDataset(dataDir) 
#                     for dataDir in train_root_path.iterdir() 
#                     if dataDir.is_dir()
#                 ])

